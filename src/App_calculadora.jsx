import { useEffect, useState } from 'react'
import reactLogo from './assets/react.svg'
import './App_calculadora.css'


function App_calculadora() {
    const [pessoas, setPessoas] = useState(1);
  
    const [valorConta, setvalorConta] = useState(0);
  
    const [porcentagem, setPorcentagem] = useState(0);
  
    const [resultadoPessoa, setResultadoPessoa] = useState(valorConta)
  
    const calculadora = () =>{
      if(valorConta != ""){
        let total_conta = parseInt(valorConta);
        let porc_gorjeta =  porcentagem === "" ? 0 : parseInt(porcentagem)
        let totalPessoa = parseInt(pessoas)
        let valorTotal = (total_conta + (valorConta * (porcentagem/100)))/resultadoPessoa
      
        setResultadoPessoa(valorTotal.toFixed(2))
      }
    }
  
    useEffect(calculadora,[valorConta,porcentagem,pessoas])
  
    const add1Pessoa = () =>{
      setPessoas(p => p+=1)
    }
  
    const remove1Pessoa = () =>{
      if(pessoas >1){
        setPessoas(p => p-=1)
      }
    }

    return (
      <main className="calculadora">
        <h2>App Calculadora</h2>
        <p>
          <label htmlFor='total_conta'>Informe o valor da conta:</label>
        </p>
        <p>
          <input type="text" id='valorConta' placeholder='R$0.00' value={valorConta} 
          onChange={(e) => setvalorConta(e.target.value)} ></input>
        </p>
        <p>
          <label htmlFor="porc_gorjeta">Gorjeta</label>
        </p>  
        <p>
          <select id="porc_gorjeta">
          <option disabled selected value={porcentagem} onChange={(e) => setPorcentagem(e.target.value)}>
            Selecione a taxa de Serviço:</option>
          <option value="0.3">30%</option>
            <option value="0.2">20%</option>
            <option value="0.15">15%</option>
            <option value="0.1">10%</option>
            <option value="0.05">5%</option>
          </select>
        </p>
        <span>Pessoas</span> <span id='valor_pessoa'></span>
        <p><button onClick={remove1Pessoa}>-</button>{pessoas}
        <button onClick={add1Pessoa}>+</button>
        <span id='resultado'>{`R$${resultadoPessoa}`}</span></p>
      </main>
    )
    
  }
  

export default App_calculadora

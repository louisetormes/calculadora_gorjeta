import React from 'react'
import ReactDOM from 'react-dom/client'
import App_calculadora from './App_calculadora'
import './App_calculadora.css'

ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
    <App_calculadora />
  </React.StrictMode>
)
